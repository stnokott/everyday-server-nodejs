import { CollectionConfig } from "payload/types";
import ProcessPhoto from "../processing";
import Media from "./Media";
import Faces from "./Faces";
import { Payload } from "payload";

const hookFaceRectAfterChange = ({ operation = "", req: { payload }, originalDoc = null, data = null }) => {
	if (operation === "update" && Object.hasOwn(data, "face_rects")) {
		payload.update({
			collection: Photos.slug,
			id: originalDoc.id,
			data: {
				is_processed: (data.face_rects.length > 0),
				needs_review: (data.face_rects.length > 1)
			},
			depth: 0
		});
	}
};

const hookAfterChange = ({ operation, req: { payload }, doc }) => {
	if (operation === "create") {
		ProcessPhoto(doc.id, payload);
	}
};

const hookAfterDelete = ({ req: { payload }, doc }) => {
	// delete appropriate media as well
	payload.delete({
		collection: Media.slug,
		id: doc.image,
		depth: 0
	});

	// delete face rects
	for (let i = 0; i < doc.face_rects.length; i++) {
		payload.delete({
			collection: Faces.slug,
			id: doc.face_rects[i],
			depth: 0
		});
	}
};

const authCheckOwnerAndAdmins = ({ req: { user } }) => {
	if (user.role === "Admin") {
		return true;
	}

	const query = {
		user: {
			equals: user.id,
		},
	};
	return query;
};

const endpointSelectRect = async (req: { params: { id: string; rect_id: string; }, payload: Payload }, res) => {
	const photoID = req.params.id;
	const selectedRectID = req.params.rect_id;

	// find photo in question
	let photo = await req.payload.findByID({
		collection: Photos.slug,
		id: photoID,
		depth: 0,
	});
	if (!photo.needs_review) {
		res.status(400).send({ error: "photo does not need review" });
		return;
	}

	// delete face rects
	for (let i = 0; i < photo.face_rects.length; i++) {
		const rect_id = photo.face_rects[i];
		if (rect_id != selectedRectID) {
			await req.payload.delete({
				collection: Faces.slug,
				id: rect_id,
				depth: 2,
			});
		}
	}

	// update photo
	photo = await req.payload.update({
		collection: Photos.slug,
		id: photoID,
		data: {
			face_rects: [selectedRectID]
		},
		depth: 0
	});
	res.status(200).send({ photo });
};

const Photos: CollectionConfig = {
	slug: "photos",
	fields: [
		{
			name: "user",
			type: "relationship",
			required: true,
			relationTo: "users",
		},
		{
			name: "day",
			type: "date",
			required: true,
			admin: {
				date: {
					pickerAppearance: "dayOnly",
					displayFormat: "dd.MM.yyyy"
				},
				description: "Date on which this photo was taken."
			},
			index: true,
		},
		{
			name: "image",
			type: "upload",
			relationTo: Media.slug,
			unique: true,
			required: true,
		},
		{
			name: "note",
			type: "text",
			required: false,
			admin: {
				description: "Note for this photo, e.g. \"Fun times in Phantasialand!\""
			},
		},
		{
			type: "row",
			fields: [
				{
					name: "needs_review",
					label: "Needs Review?",
					type: "checkbox",
					defaultValue: false,
					index: true,
					admin: {
						description: "Indicates whether multiple face rectangles are available from which the user needs to choose one. (read-only)",
						readOnly: true
					}
				},
				{
					name: "is_processed",
					label: "Is Processed?",
					type: "checkbox",
					defaultValue: false,
					index: true,
					admin: {
						description: "Indicates whether this photo has been processed by the backend. (read-only)",
						readOnly: true
					}
				}
			]
		},
		{
			type: "relationship",
			name: "face_rects",
			label: "Faces",
			relationTo: Faces.slug,
			hasMany: true,
			index: true,
			admin: {
				description: "Possible faces after detection (read-only).",
				// readOnly: true,
			},
			hooks: {
				afterChange: [hookFaceRectAfterChange],
			},
		}
	],
	endpoints: [
		{
			path: "/:id/select_rect/:rect_id",
			method: "put",
			// @ts-expect-error (required because VSCode doesn't recognize payload parameter)
			handler: endpointSelectRect
		}
	],
	hooks: {
		afterDelete: [hookAfterDelete],
		afterChange: [hookAfterChange]
	},
	access: {
		read: authCheckOwnerAndAdmins,
		update: authCheckOwnerAndAdmins,
		delete: authCheckOwnerAndAdmins
	},
	admin: {
		disableDuplicate: true
	},
	timestamps: true,
};

export default Photos;
