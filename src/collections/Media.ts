import { CollectionConfig } from "payload/types";

const Media: CollectionConfig = {
	slug: "media",
	fields: [],
	upload: {
		staticURL: "/media",
		staticDir: "media",
		imageSizes: [
			{
				name: "thumbnail",
				width: 250,
				height: 250,
				crop: "centre"
			}
		],
		adminThumbnail: "thumbnail",
		mimeTypes: ["image/*"]
	},
	timestamps: true,
	admin: {
		disableDuplicate: true,
	},
};

export default Media;
