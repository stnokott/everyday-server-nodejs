import { CollectionConfig } from "payload/types";

const Faces: CollectionConfig = {
	slug: "faces",
	fields: [
		{
			name: "x0",
			label: "x0",
			type: "number",
			required: true,
			admin: {
				description: "X-coordinate of top left corner of the face bounding box.",
				readOnly: true
			}
		},
		{
			name: "y0",
			label: "y0",
			type: "number",
			required: true,
			admin: {
				description: "Y-coordinate of top left corner of the face bounding box.",
				readOnly: true
			}
		},
		{
			name: "x1",
			label: "x1",
			type: "number",
			required: true,
			admin: {
				description: "X-coordinate of bottom right corner of the face bounding box.",
				readOnly: true
			}
		},
		{
			name: "y1",
			label: "y1",
			type: "number",
			required: true,
			admin: {
				description: "Y-coordinate of bottom right corner of the face bounding box.",
				readOnly: true
			}
		},
		{
			name: "score",
			type: "number",
			required: true,
			admin: {
				description: "Confidence that this bounding box represents a face (0.0-1.0). Higher is better.",
				readOnly: true
			}
		},
		{
			name: "emotion",
			type: "select",
			options: [
				"angry",
				"disgusted",
				"fearful",
				"happy",
				"neutral",
				"sad",
				"surprised",
			],
			admin: {
				description: "Most likely emotion shown by the person on the photo.",
				readOnly: true
			}
		}
	],
	timestamps: true,
	access: {
		create: () => false,
		update: () => false,
	},
};

export default Faces;
