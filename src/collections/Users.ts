import { CollectionConfig } from "payload/types";

export const AuthCheckAdminsOnly = ({ req: { user } }) => {
	return user.role === "Admin";
};

const authCheckOwnerAndAdmins = ({ req: { user } }) => {
	if (user.role === "Admin") {
		return true;
	}

	const query = {
		id: {
			equals: user.id,
		},
	};
	return query;
};

const Users: CollectionConfig = {
	slug: "users",
	auth: true,
	fields: [
		// Email added by default
		{
			name: "name",
			type: "text",
		},
		{
			name: "role",
			type: "select",
			options: [
				"User",
				"Admin"
			],
			defaultValue: "User",
			access: {
				update: AuthCheckAdminsOnly
			}
		}
	],
	admin: {
		useAsTitle: "name",
	},
	access: {
		create: AuthCheckAdminsOnly,
		delete: AuthCheckAdminsOnly,
		read: authCheckOwnerAndAdmins
		// admin: AuthCheckAdminsOnly // disables any admin UI interaction from non-admin users
	},
};

export default Users;
