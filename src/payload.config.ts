import { buildConfig } from "payload/config";
import path from "path";
import Photos from "./collections/Photos";
import Media from "./collections/Media";
import Faces from "./collections/Faces";
import Users from "./collections/Users";

const processingFilepath = path.resolve(__dirname, "processing.ts");
const mockModulePath = path.resolve(__dirname, "mocks/emptyObject.ts");

export default buildConfig({
	serverURL: "http://localhost:3000",
	admin: {
		user: Users.slug,
		dateFormat: "dd.MM.yyyy HH:mm",
		webpack: (config) => ({
			...config,
			resolve: {
				...config.resolve,
				alias: {
					...config.resolve.alias,
					[processingFilepath]: mockModulePath,
				},
			}
		})
	},
	collections: [
		Users,
		Photos,
		Media,
		Faces
	],
	typescript: {
		outputFile: path.resolve(__dirname, "payload-types.ts")
	},
	graphQL: {
		disable: true
	},
});
