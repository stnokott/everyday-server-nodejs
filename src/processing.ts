import * as faceapi from "@vladmandic/face-api";
import payload, { Payload } from "payload";
import { IncomingUploadType } from "payload/dist/uploads/types";
import Media from "./collections/Media";
import Photos from "./collections/Photos";
import path from "path";
import fs from "fs";
import Faces from "./collections/Faces";

const MODEL_URL = "./weights";

export const InitFaceAPI = async (pl: Payload) => {
	const startTime = Date.now();
	await faceapi.nets.ssdMobilenetv1.loadFromDisk(MODEL_URL);
	await faceapi.nets.faceLandmark68Net.loadFromDisk(MODEL_URL);
	await faceapi.nets.faceExpressionNet.loadFromDisk(MODEL_URL);
	pl.logger.info("Face detection models loaded from disk in " + (Date.now() - startTime) + "ms.");
};

const faceAPIOptions = new faceapi.SsdMobilenetv1Options({ minConfidence: 0.1, maxResults: 10 });

const ProcessPhoto = async (id: string, pl: Payload) => {
	const photo = await pl.findByID({
		collection: Photos.slug,
		id: id,
		depth: 1,
	});
	const mediaDirName = (Media.upload as IncomingUploadType).staticURL;
	const imgPath = path.join(__dirname, mediaDirName, photo.image.filename);

	const buffer = fs.readFileSync(imgPath);
	// @ts-expect-error (faceapi TensorFlow API not correctly detected by VSCode)
	const decodeT = faceapi.tf.node.decodeImage(buffer, 3);
	const expandT = faceapi.tf.expandDims(decodeT, 0);
	// @ts-expect-error (see above)
	const detections = await faceapi.detectAllFaces(expandT, faceAPIOptions).withFaceLandmarks().withFaceExpressions();
	// @ts-expect-error (see above)
	faceapi.tf.dispose([decodeT, expandT]);

	const faceRectIDs = [];
	for (let i = 0; i < detections.length; i++) {
		const detection = detections[i].detection;
		// const landmarks = detections[i].landmarks;
		const expression = detections[i].expressions;

		const face = detection.box;
		const faceRectDB = await payload.create({
			collection: Faces.slug,
			data: {
				x0: face.topLeft.x,
				y0: face.topLeft.y,
				x1: face.bottomRight.x,
				y1: face.bottomRight.y,
				score: detection.score,
				emotion: expression.asSortedArray().at(0).expression
			}
		});
		faceRectIDs.push(faceRectDB.id);
	}

	await payload.update({
		collection: Photos.slug,
		id: id,
		data: {
			face_rects: faceRectIDs
		},
		depth: 0
	});
};

export default ProcessPhoto;
